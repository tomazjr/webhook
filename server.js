"use strict";

var express = require('express');
var app = express(); // app needs to be visible
var router = express.Router();
var bodyParser = require('body-parser');
var server = require('http').Server(app);

var io = require('socket.io')(server);
var cfg = require("./webhook.conf.json");
var jwt = require('jsonwebtoken');

var notificationService = require('./services/notificationService')(io);

var application = require('./application')(app, cfg, notificationService);
var appHandler = application.initialize(process.env.NODE_ENV);
require('./utils/extensionsMethods');
// Assign misc. Express middleware handlers
// NEW: Configure bodyParser to allow much larger POST submissions.
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: true, limit: '50mb'}));
app.use(express.static(__dirname + cfg.static));

app.all('*', function (req, res, next) {
  console.log('called ' + req.method);
  // CORS headers
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // Set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});

var routes = require('./routes')(router, appHandler, cfg);
routes.initialize();

app.use("/api/", function (req, res, next) {
  var jr = {"status": "bad", "msg": "Failed to authenticate token. "};
  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  // decode token
  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, cfg.secret, function(err, decoded) {
      if (err) { console.log(jr.msg); return res.json(jr); } else {
        // if everything is ok
        req.currentUser = decoded._doc;
        next();
      }
    });

  } else {
    // if there is no token
    // return an error
    jr.msg+='No token provided.';
    console.log(jr.msg);
    return res.json(jr);
  }
});
//// If no route is matched by now, it must be a 404
//app.use(function(req, res, next) {
//  var err = new Error('Not Found');
//  err.status = 404;
//  res.status(404).send('404 - url not found').end();
//});

app.use('/', router);

server.listen(cfg.htpport, function () {
  console.log('HootSuite - webhook pid %s listening on %d in %s',
    process.pid, cfg.htpport,
    process.env.NODE_ENV);
});

module.exports = app;