var mongoose = require('mongoose');
var http = require('http');
var bcrypt   = require('bcrypt-nodejs');

module.exports = function (app, cfg, notificationService) {

  function getMongodbConn() {
    var connStr = 'mongodb://' + cfg.mongo.host + '/' + cfg.mongo.db;
    console.log('mongo conn:', connStr);
    return mongoose.connect('mongodb://' + cfg.mongo.host + '/' + cfg.mongo.db);
  }

  function getMongodbModels() {

    var UserSchema = new mongoose.Schema({
      email: {type:String, required: [true, 'email is required']},
      password: {type:String, required: [true, 'password is required']},
    });

    // generate hash
    UserSchema.methods.generateHash = function(password) {
      return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    };

    // checking if password is valid
    UserSchema.methods.validPassword = function(password) {
      return bcrypt.compareSync(password, this.password);
    };

    var DestinationSchema = new mongoose.Schema({
      userId:{type:String},
      protocol: {type:String, default:"http:"},
      port: {type:Number},
      url: {type: String, required: [true, 'Url is required']},
      creation_date: {type: Date, default: Date.now},
      last_progress_attempt: Date,
      in_progress:{type: Boolean, default: false},
      messages: {type: Array, default: []}
    });

    DestinationSchema.statics.findAndModify = function (query, sort, update, callback) {
      return this.collection.findAndModify(query, sort, update, callback);
    };

    var MessageSchema = new mongoose.Schema({
      protocol: {type:String},
      port: {type:Number},
      path: String,
      method: {type: String, required: [true, 'Method is required'], validate: {
        validator: function(value) {
          return http.METHODS.indexOf(value) !== -1;
        },
        message: '{VALUE} is not a valid method!'
      }},
      headers: Object,
      creation_date: {type: Date, default: Date.now},
      attempts: {type:Number, default:0},
      last_attempt: Date,
      status: Number,
      body: Object
    });

    var LogSchema = new mongoose.Schema({
      date: {type: Date, default: Date.now},
      statusCode: Number,
      data: Object
    });

    return {
      Destination: mongoose.model('Destination', DestinationSchema),
      Message: mongoose.model('Message', MessageSchema),
      User: mongoose.model('User', UserSchema),
      LogSchema: LogSchema
    }
  }

  function initialize() {
    var mongoDbConn = getMongodbConn();
    if(app) {
      // secret variable
      app.set('superSecret', cfg.secret);

      app.use(function (req, res, next) {
        req.mongoDbConn = mongoDbConn;
        req.notify = notificationService;

        next();
      });
    }
    // should be used to initialize third-part libraries
    return {
      mongoDbConn: mongoDbConn,
      mongoModels: getMongodbModels()
    }

  }

  return {
    initialize: initialize
  }
};