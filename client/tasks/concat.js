var gulp          = require('gulp'),
    concat        = require('gulp-concat');

gulp.task('concat', function() {
  return gulp.src(gulp.paths.scripts)
    .pipe(concat('application.js'))
    .pipe(gulp.dest(gulp.paths.dist))
});