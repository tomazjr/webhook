angular.module('webhook').controller('LoginController', ['$scope', '$location', 'localStorageService', 'userService', 
function($scope, $location, localStorageService, userService) {

  $scope.authenticate = function(user) {
    userService.authenticate(user).then(function (result) {
      if (localStorageService.set('token', result.data.data))
        $location.url("destinations/");
    });
  }

  $scope.register = function(user) {
    userService.register(user).then(function (result) {
      $scope.authenticate(user);
    });
  }

}])