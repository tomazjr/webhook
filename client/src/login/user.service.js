angular.module('webhook').service('userService', ['webhookService', function (webhookService) {
  var self = this;

  this.all = function() {
    return webhookService.get("users");
  }

  this.create = function(user) {
    return webhookService.post("user/", user);
  }

  this.authenticate = function(user) {
    return webhookService.post("user/authenticate/", user);
  }

  this.register = function(user) {
    return webhookService.post("user/", user);
  }

}]);