angular.module('webhook').controller('DestinationsController', ['$scope', '$location', 'destinationService', 'webhookConfig',
function($scope, $location, destinationService, webhookConfig) {
  $scope.protocols = webhookConfig.protocols;

  destinationService.all().then(function(result) {
    $scope.destinations =  result.data.data;
  });

  $scope.detail = function(dest) {
    $scope.dest = dest || { protocol: $scope.protocols[0] };
    $("#post-modal").modal('show');
  };

  $scope.save = function(dest) {
    destinationService.save(dest).then(function(result) {
      if (!dest._id) {
        dest._id = result.data.data.id;
        $scope.destinations.push(dest);
      }
      $scope.dest = null;
    })
  };

  $scope.gotoMessages = function(dest) {
    $location.url("destination/"+dest._id);
  };

  $scope.remove = function(dest) {
    destinationService.delete(dest).then(function(result) {
      var index = $scope.destinations.indexOf(dest);
      $scope.destinations.splice(index, 1);
    })
  }
}]);