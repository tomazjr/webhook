angular.module('webhook').service('destinationService', ['$http', 'webhookService', 
function ($http, webhookService) {
  var self = this;

  this.all = function() {
    return webhookService.get("api/destinations");
  }

  this.get = function(id) {
    return webhookService.get("api/destination/" + id);
  }

  this.put = function(dest) {
    return webhookService.put("api/destination/"+dest._id, dest);
  }

  this.post = function(dest) {
    return webhookService.post("api/destination/", dest);
  }

  this.save = function(dest) {
    if (dest._id)
      return self.put(dest);
    else
      return self.post(dest);
  }

  this.delete = function(dest) {
    return webhookService.delete("api/destination/" + dest._id);
  }
}]);