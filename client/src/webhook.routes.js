angular.module('webhook').config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/destinations', { 
            templateUrl: 'src/destination/destinations.html', 
            controller: 'DestinationsController'})
        .when('/destination/:id', { 
            templateUrl: 'src/message/messages.html', 
            controller: 'MessagesController'})
        .when('/destination/:destinationId/message/:messageId', { 
            templateUrl: 'src/log/logs.html',
            controller: 'LogsController'})
        .when('/login', { 
            templateUrl: 'src/login/login.html',
            controller: 'LoginController'})
        .when('/', { redirectTo: '/destinations' })
        .otherwise({
          templateUrl: 'src/pages/404.html',
        });
}]);