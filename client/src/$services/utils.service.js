angular.module('webhook').service('utils', function() {
  return { 
    statusColor: function (code) {
      if (code >= 200 && code < 400)
        return 'success'
      if (code >= 400 && code < 500)
        return 'warning'
      if (code >= 500)
        return 'danger'
    }
  }
});