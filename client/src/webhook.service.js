angular.module('webhook').service('webhookService', ['$http', 'webhookConfig', 'localStorageService', 
function ($http, webhookConfig, localStorageService) {
  this.get = function(url) {
    return $http({
      method: 'GET',
      headers: {
        'Accept': webhookConfig.accept,
        'x-access-token': localStorageService.get('token')
      },
      url: webhookConfig.server.url + url
    });
  }

  this.post = function(url, data) {
    return $http({
      method: 'POST',
      headers: {
        'Accept': webhookConfig.accept,
        'x-access-token': localStorageService.get('token')
      },
      data: data,
      url: webhookConfig.server.url + url
    });
  }

  this.put = function(url, data) {
    return $http({
      method: 'PUT',
      headers: {
        'Accept': webhookConfig.accept,
        'x-access-token': localStorageService.get('token')
      },
      data: data,
      url: webhookConfig.server.url + url
    });
  }

  this.delete = function(url, data) {
    return $http({
      method: 'DELETE',
      headers: {
        'Accept': webhookConfig.accept,
        'x-access-token': localStorageService.get('token')
      },
      url: webhookConfig.server.url + url
    });
  }
}]);