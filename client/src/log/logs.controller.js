angular.module('webhook').controller('LogsController', ['$scope', '$routeParams', 'messageService', 'utils', 'toaster',
function($scope, $routeParams, messageService, utils, toaster) {

  messageService.get($routeParams.destinationId, $routeParams.messageId).then(function(result) {
    $scope.message =  result.data.data;
  });

  $scope.getItemClass = function(log) {
    return 'list-group-item-' + utils.statusColor(log.statusCode)
  }

  $scope.exec = function() {
    messageService.exec($routeParams.destinationId, $routeParams.messageId).then(function(result) {
      toaster.pop("info", null, "Message executed!");
      $scope.message.logs.push(result.data.data);
    });
  }
}])