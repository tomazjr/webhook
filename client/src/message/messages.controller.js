angular.module('webhook').controller('MessagesController', ['$scope', '$routeParams', '$location', 'destinationService',
 'messageService', 'webhookConfig', 'utils', 'toaster',
function($scope, $routeParams, $location, destinationService, messageService, webhookConfig, utils, toaster) {
  $scope.protocols = webhookConfig.protocols;
  $scope.verbs = webhookConfig.verbs;

  destinationService.get($routeParams.id).then(function(result) {
    $scope.destination =  result.data.data;
  });

  $scope.detail = function(msg) {
    msg = msg || { protocol: $scope.destination.protocol, method: $scope.verbs[0], headers: {} }
    msg.port = msg.protocol === 'https:' ? "443" : "80"
    $scope.msg = msg;
    $("#message-modal").modal('show');
  }

  $scope.getPanelColor = function(msg) {
    return 'panel-' + utils.statusColor(msg.status);
  }

  $scope.getBaseUrl = function(msg) {
    if (msg) {
      var port = (msg.port != 80) && (msg.port != 443)  ? ':'+msg.port : '' 
      return msg.protocol+"//"+$scope.destination.url+port
    }
      
  }

  $scope.saveHeader = function(msg, key, value) {
    msg.headers[key] = value;
    $scope.newHeader = null
  }

  $scope.save = function(msg) {
    messageService.post($scope.destination._id, msg).then(function(result) {
      $scope.destination.messages.push(result.data.data);
      $scope.msg = null;
    });
  }

  $scope.exec = function(msg) {
    messageService.exec($scope.destination._id, msg._id).then(function(result) {
      toaster.pop("info", null, "Message executed!");
    });
  }

  $scope.gotoLogs = function(msg) {
    $location.url("destination/"+$scope.destination._id+"/message/"+msg._id);
  }

  $scope.remove = function(msg) {
    messageService.delete(msg).then(function(result) {
      var index = $scope.destinations.indexOf(dest);
      $scope.destinations.splice(index, 1);
    })
  }
}])