angular.module('webhook').service('messageService', ['$http', 'webhookService', function ($http, webhookService) {
  var self = this;

  this.get = function(destinationId, messageId) {
    var url = "api/destination/" + destinationId + "/message/" + messageId;
    return webhookService.get(url);
  }

  this.post = function(destinationId, msg) {
    return webhookService.post("api/destination/"+destinationId+"/message", msg);
  }

  this.exec = function(destinationId, msgId) {
    var url = "api/destination/"+destinationId+"/message/"+msgId+"/exec";
    return webhookService.put(url);
  }
}]);