angular.module("webhook").value("webhookConfig", {
	server: {
        url: "http://localhost:3010/",
        accept: "application/json"
    },
    protocols: ['http:', 'https:'],
    verbs: ['GET', 'POST', 'PUT', 'DELETE', 'PATCH']
});