angular.module('webhook', [
    'ngRoute',
    'LocalStorageModule',
    'toaster'
])
angular.module("webhook").value("webhookConfig", {
	server: {
        url: "http://localhost:3010/",
        accept: "application/json"
    },
    protocols: ['http:', 'https:'],
    verbs: ['GET', 'POST', 'PUT', 'DELETE', 'PATCH']
});
angular.module('webhook').config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/destinations', { 
            templateUrl: 'src/destination/destinations.html', 
            controller: 'DestinationsController'})
        .when('/destination/:id', { 
            templateUrl: 'src/message/messages.html', 
            controller: 'MessagesController'})
        .when('/destination/:destinationId/message/:messageId', { 
            templateUrl: 'src/log/logs.html',
            controller: 'LogsController'})
        .when('/login', { 
            templateUrl: 'src/login/login.html',
            controller: 'LoginController'})
        .when('/', { redirectTo: '/destinations' })
        .otherwise({
          templateUrl: 'src/pages/404.html',
        });
}]);
angular.module('webhook').service('webhookService', ['$http', 'webhookConfig', 'localStorageService', 
function ($http, webhookConfig, localStorageService) {
  this.get = function(url) {
    return $http({
      method: 'GET',
      headers: {
        'Accept': webhookConfig.accept,
        'x-access-token': localStorageService.get('token')
      },
      url: webhookConfig.server.url + url
    });
  }

  this.post = function(url, data) {
    return $http({
      method: 'POST',
      headers: {
        'Accept': webhookConfig.accept,
        'x-access-token': localStorageService.get('token')
      },
      data: data,
      url: webhookConfig.server.url + url
    });
  }

  this.put = function(url, data) {
    return $http({
      method: 'PUT',
      headers: {
        'Accept': webhookConfig.accept,
        'x-access-token': localStorageService.get('token')
      },
      data: data,
      url: webhookConfig.server.url + url
    });
  }

  this.delete = function(url, data) {
    return $http({
      method: 'DELETE',
      headers: {
        'Accept': webhookConfig.accept,
        'x-access-token': localStorageService.get('token')
      },
      url: webhookConfig.server.url + url
    });
  }
}]);
angular.module('webhook').service('utils', function() {
  return { 
    statusColor: function (code) {
      if (code >= 200 && code < 400)
        return 'success'
      if (code >= 400 && code < 500)
        return 'warning'
      if (code >= 500)
        return 'danger'
    }
  }
});
angular.module('webhook').service('destinationService', ['$http', 'webhookService', 
function ($http, webhookService) {
  var self = this;

  this.all = function() {
    return webhookService.get("api/destinations");
  }

  this.get = function(id) {
    return webhookService.get("api/destination/" + id);
  }

  this.put = function(dest) {
    return webhookService.put("api/destination/"+dest._id, dest);
  }

  this.post = function(dest) {
    return webhookService.post("api/destination/", dest);
  }

  this.save = function(dest) {
    if (dest._id)
      return self.put(dest);
    else
      return self.post(dest);
  }

  this.delete = function(dest) {
    return webhookService.delete("api/destination/" + dest._id);
  }
}]);
angular.module('webhook').controller('DestinationsController', ['$scope', '$location', 'destinationService', 'webhookConfig',
function($scope, $location, destinationService, webhookConfig) {
  $scope.protocols = webhookConfig.protocols;

  destinationService.all().then(function(result) {
    $scope.destinations =  result.data.data;
  });

  $scope.detail = function(dest) {
    $scope.dest = dest || { protocol: $scope.protocols[0] };
    $("#post-modal").modal('show');
  };

  $scope.save = function(dest) {
    destinationService.save(dest).then(function(result) {
      if (!dest._id) {
        dest._id = result.data.data.id;
        $scope.destinations.push(dest);
      }
      $scope.dest = null;
    })
  };

  $scope.gotoMessages = function(dest) {
    $location.url("destination/"+dest._id);
  };

  $scope.remove = function(dest) {
    destinationService.delete(dest).then(function(result) {
      var index = $scope.destinations.indexOf(dest);
      $scope.destinations.splice(index, 1);
    })
  }
}]);
angular.module('webhook').controller('LogsController', ['$scope', '$routeParams', 'messageService', 'utils', 'toaster',
function($scope, $routeParams, messageService, utils, toaster) {

  messageService.get($routeParams.destinationId, $routeParams.messageId).then(function(result) {
    $scope.message =  result.data.data;
  });

  $scope.getItemClass = function(log) {
    return 'list-group-item-' + utils.statusColor(log.statusCode)
  }

  $scope.exec = function() {
    messageService.exec($routeParams.destinationId, $routeParams.messageId).then(function(result) {
      toaster.pop("info", null, "Message executed!");
      $scope.message.logs.push(result.data.data);
    });
  }
}])
angular.module('webhook').controller('LoginController', ['$scope', '$location', 'localStorageService', 'userService', 
function($scope, $location, localStorageService, userService) {

  $scope.authenticate = function(user) {
    userService.authenticate(user).then(function (result) {
      if (localStorageService.set('token', result.data.data))
        $location.url("destinations/");
    });
  }

  $scope.register = function(user) {
    userService.register(user).then(function (result) {
      $scope.authenticate(user);
    });
  }

}])
angular.module('webhook').service('userService', ['webhookService', function (webhookService) {
  var self = this;

  this.all = function() {
    return webhookService.get("users");
  }

  this.create = function(user) {
    return webhookService.post("user/", user);
  }

  this.authenticate = function(user) {
    return webhookService.post("user/authenticate/", user);
  }

  this.register = function(user) {
    return webhookService.post("user/", user);
  }

}]);
angular.module('webhook').service('messageService', ['$http', 'webhookService', function ($http, webhookService) {
  var self = this;

  this.get = function(destinationId, messageId) {
    var url = "api/destination/" + destinationId + "/message/" + messageId;
    return webhookService.get(url);
  }

  this.post = function(destinationId, msg) {
    return webhookService.post("api/destination/"+destinationId+"/message", msg);
  }

  this.exec = function(destinationId, msgId) {
    var url = "api/destination/"+destinationId+"/message/"+msgId+"/exec";
    return webhookService.put(url);
  }
}]);
angular.module('webhook').controller('MessagesController', ['$scope', '$routeParams', '$location', 'destinationService',
 'messageService', 'webhookConfig', 'utils', 'toaster',
function($scope, $routeParams, $location, destinationService, messageService, webhookConfig, utils, toaster) {
  $scope.protocols = webhookConfig.protocols;
  $scope.verbs = webhookConfig.verbs;

  destinationService.get($routeParams.id).then(function(result) {
    $scope.destination =  result.data.data;
  });

  $scope.detail = function(msg) {
    msg = msg || { protocol: $scope.destination.protocol, method: $scope.verbs[0], headers: {} }
    msg.port = msg.protocol === 'https:' ? "443" : "80"
    $scope.msg = msg;
    $("#message-modal").modal('show');
  }

  $scope.getPanelColor = function(msg) {
    return 'panel-' + utils.statusColor(msg.status);
  }

  $scope.getBaseUrl = function(msg) {
    if (msg) {
      var port = (msg.port != 80) && (msg.port != 443)  ? ':'+msg.port : '' 
      return msg.protocol+"//"+$scope.destination.url+port
    }
      
  }

  $scope.saveHeader = function(msg, key, value) {
    msg.headers[key] = value;
    $scope.newHeader = null
  }

  $scope.save = function(msg) {
    messageService.post($scope.destination._id, msg).then(function(result) {
      $scope.destination.messages.push(result.data.data);
      $scope.msg = null;
    });
  }

  $scope.exec = function(msg) {
    messageService.exec($scope.destination._id, msg._id).then(function(result) {
      toaster.pop("info", null, "Message executed!");
    });
  }

  $scope.gotoLogs = function(msg) {
    $location.url("destination/"+$scope.destination._id+"/message/"+msg._id);
  }

  $scope.remove = function(msg) {
    messageService.delete(msg).then(function(result) {
      var index = $scope.destinations.indexOf(dest);
      $scope.destinations.splice(index, 1);
    })
  }
}])