

module.exports = function (router, appHandler, cfg) {

  function initialize() {
    var destinationSvc= require('./services/destinationService')(appHandler.mongoModels);
    router.post('/api/destination', destinationSvc.create);
    router.delete('/api/destination/:id', destinationSvc.remove);
    router.put('/api/destination/:id', destinationSvc.update);
    router.get('/api/destination/:id', destinationSvc.get);
    router.get('/api/destinations', destinationSvc.getAll);

    var messageSvc = require('./services/messageService')(appHandler.mongoModels);
    router.post('/api/destination/:id/message', messageSvc.create);
    router.put('/api/destination/:id/message/:messageId/exec', messageSvc.execute);
    router.get('/api/destination/:id/message/:messageId', messageSvc.get);

    var userSvc = require('./services/userService')(appHandler.mongoModels,cfg);
    router.get('/api/users', userSvc.getAll);
    router.post('/user/authenticate', userSvc.authenticate);
    router.post('/user', userSvc.create);

    var apiHelperSvc = require('./services/apiHelperService')(cfg);
    router.get('/', apiHelperSvc.ping);
    router.get('/help', apiHelperSvc.help);

  }

  return {
    initialize: initialize
  }
};