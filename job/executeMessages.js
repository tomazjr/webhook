var cfg = require("../webhook.conf.json");
var app = null; // no exoress
var application = require('../application')(app, cfg);
var moment = require("moment");
appHandle = application.initialize(process.env.NODE_ENV);
var models = appHandle.mongoModels;
async = require('async');
var DEFAULT_POOL_SIZE = 5; //
var hasItems = true;

// create a queue object with concurrency POOL_SIZE
var queue = async.queue(function (destination, callback) {

  var child = require('child_process').fork('./job/worker');
  child.send(destination);
  child.on('message', function(message) {
    if (message.err) { console.log("Err: ", message.err)}
    console.log("server: ", message.destination);
    models.Destination.update({'_id':message.destination._id},
      {
        $set:{'in_progress':false}
      }, function (err, result) {
        if (err) { console.log("err:", err); reject(err); }
        console.log("destination", message.destination._id, "executed");
        console.log("killing child process calling: kill -15", child.pid);
        child.kill(); //send the 'SIGTERM' signal to child process.
        callback()
      });
  });
}, DEFAULT_POOL_SIZE);

queue.drain = function () {
  console.log("queue is drained");
  if (!hasItems) { process.exit(0) }
};

var nowTime = moment.utc(new Date()); // current utc-time
console.log("starting execute pending messages script :", nowTime.format());
var nowIso = nowTime.toISOString();
var oneHourLess = moment(nowTime).subtract(1, 'hour');

// get pending destinationmongom
var query = {
  'in_progress' : false,
  '$where':'this.messages.length > 0',
  'messages' : { $elemMatch: { status: { $ne: 200 } } },
  $or : [{'last_progress_attempt' : {$exists:false}}, {'last_progress_attempt' : {$lt : new Date(oneHourLess)}}]
};
var sort = { last_progress_attempt : 1 };
var update =  { $set : { in_progress : true, last_progress_attempt : nowIso } };
var hasAtListOneItem = false;
var interval = setInterval(function(){
  models.Destination.findAndModify(query, sort, update, function (err, data) {
    if (err) { console.log("error: ", err); process.exit(1); }
    console.log(data);
    if (data.value) {
      hasAtListOneItem = true;
      queue.push(data.value);
    } else {
      console.log("no object to update");
      hasItems = false;
      clearInterval(interval);
      if (!hasAtListOneItem) { process.exit(0); }
    }
  });
}, 1000);










