var cfg = require("../webhook.conf.json");
var app = null; // no exoress
var application = require('../application')(app, cfg);
var moment = require("moment");
appHandle = application.initialize(process.env.NODE_ENV);
var models = appHandle.mongoModels;
var mongoDbConn = appHandle.mongoDbConn;
async = require('async');
var messageSvc = require('../services/messageService')(models);

var nowTime = moment.utc(new Date()); // current utc-time
console.log("time:", nowTime.format());

var oneHourLess = moment(nowTime).subtract(1, 'hour');
console.log("oneHourLess:", oneHourLess.format());

process.on('message', function(destination) {
  console.log("child:", destination);
  // destination = JSON.parse(destination);
  execute(destination._id, function (err) {
    process.send({err:err, destination:destination}); // send message back to parent process
  });
});

function execute(destId, next) {
  console.log("search for: ", destId);
  models.Destination.findById(destId, function (err, destination) {
    if (!destination) { return next("destination not found", destination); }
    console.log("object to update,", destination);

    async.map(destination.messages.filter(function (message) {
      return message.status != 200;
    }), function (message, callback) {
      console.log("message:", message);
      messageSvc.sendMessage(destination, message, mongoDbConn).then(function (result) {
        callback(null, result);
      }, function (err) {
        callback(err);
      })
    }, function (err, result) {
      console.log("execute err:", err);
      next(err, destination);
    })
  })
}

