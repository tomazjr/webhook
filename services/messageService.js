var utils = require('../utils/util');
var promise = require('bluebird');
var http = require('http');
var https = require('https');
var _ = require('underscore');
async = require('async');
var contentType = require('content-type');
var url = require('url');

module.exports = function (models) {

  var logSvc = require('./logService')(models);

  function _mountRequestOptions(destination, message) {

    var options = JSON.parse(JSON.stringify(message));
    options.hostname = destination.url;

    var notUseFields = ['_id', 'attempts', 'creation_date', 'last_attempt', 'status', 'body', 'protocol'];
    notUseFields.forEach(function (field) {
      delete options[field];
    });

    return options;
  }

  function _getProtocolLibrary(value) {
    return value == "https:" ? https : http;
  }

  function _updateStats(message, sendMessageResult, mongoDbConn, callback) {

    async.parallel([
      function (cb) { logSvc.create(message, sendMessageResult, mongoDbConn, cb) },
      function (cb) {
        models.Destination.update({'messages._id':message._id},
          {
            $inc:{'messages.$.attempts':1},
            $set:{'messages.$.last_attempt':new Date(), 'messages.$.status':sendMessageResult.statusCode}
          }, cb);

        // message.last_attempt = new Date();
        // message.attempts++;
        // message.status = sendMessageResult.statusCode;
        // destination.markModified('messages');
        // destination.save(cb);
      }
    ], callback);

  }
  
  function _sendRequest(protocolLibrary, postData, opts, redirectCount) {

    redirectCount = redirectCount || 0;
    if (redirectCount > 10) {
      throw new Error("Redirected too many times.");
    }

    return new promise (function (resolve, reject) {
      var jr = {};
      var dataParts = [];

      var req = protocolLibrary.request(opts, function(res) {

        var redirectsTo;

        // http redirect
        if (res.statusCode < 400 && res.statusCode >= 300) {
          redirectsTo = res.headers["location"];
          console.log("redirect to ", redirectsTo);
          var urlParsed = url.parse(redirectsTo);
          if (urlParsed.protocol) { opts.protocol = urlParsed.protocol; }
          if (urlParsed.hostname) { opts.hostname = urlParsed.host; }
          if (urlParsed.path) { opts.path = urlParsed.path; }
          resolve({redirect: redirectsTo, opts: opts});
          return
        }

        jr.statusCode = res.statusCode;
        var contentTypeObj = contentType.parse(res.headers["content-type"]);
        var encode = 'utf8';
        // if (contentTypeObj.parameters && contentTypeObj.parameters.charset) {
        //   encode = contentTypeObj.parameters.charset;
        // }
        //
        var resContentType = contentTypeObj.type;
        res.setEncoding(encode);

        res.on('data', function (data) {
          dataParts.push(data);
        });

        res.on('end', function () {
          // var data = Buffer.concat(dataParts).toString();
          var data = _.reduce(dataParts, function (previousValue, currentValue) {
            return previousValue.concat(currentValue);
          });

          if (resContentType == "application/json") {
            jr.data = JSON.parse(data);
          } else {
            jr.data = data;
          }
          resolve({redirect: false, opts: jr});
        });
      });

      req.on('error', function(e) {
        console.log("problem with request", e.message);
        jr.data = e.message;
        resolve({redirect: false, opts: jr});
      });
      // write data to request body
      if (postData) {
        if (typeof postData == "object") {
          req.write(JSON.stringify(postData));
        } else {
          req.write(postData.toString());
        }
      }
      req.end();

    }).then(function (result) {
      return result.redirect
        ? _sendRequest(protocolLibrary, postData, result.opts, redirectCount + 1)
        : result.opts;
    })
  }
  
  function sendMessage(destination, message, mongoDbConn) {
    return new promise(function (resolve, reject) {
      var messageModel = new models.Message(message);

      messageModel.validate(function(error) {
        if (!error) {
          var postData = message.body;
          var protocolLibrary = _getProtocolLibrary(message.protocol);
          var opts = _mountRequestOptions(destination, message);
          _sendRequest(protocolLibrary, postData, opts).then(function (result) {
            _updateStats(message, result, mongoDbConn, function (err, data) {
              resolve(result);
            });
          }, function (err) {
            reject(err);
          });
        } else {
          reject(new Error("Error sending message. " + utils.mongooseErrorToMessages(error)));
        }
      });
    });
  }

  function create(req, res) {
    var jr = {"status": "bad", "msg": "fail creating a new message. "};
    var body = req.body;

    var destId = req.params.id;
    if (!utils.objectIdIsValid(destId)) { jr.msg += "destination id does not exist" ; console.log(jr.msg); res.json(jr); return; }

    console.log("message body",  body);
    models.Destination.findOne({_id:destId, userId: req.currentUser._id},function (err, destination) {
      if (err) { throw err; }
      console.log('Destination found : ', destination);
      if (!destination) { jr.msg += "destination not found" ; console.log(jr.msg); res.json(jr); return; }
      //override with destination info
      if (!body.protocol) { body.protocol = destination.protocol }
      if (!body.port) { body.port = destination.port }

      var message = new models.Message(body);

      message.validate(function(error) {
        if (!error) {
          destination.messages.push(message);
          destination.save(function (err, destination) {
            if (err) { jr.msg += err ; console.log(jr.msg); res.json(jr); return; }

            sendMessage(destination, message, req.mongoDbConn).then(function (sendMessageResult) {
              if (err) { jr.msg += err ; console.log(jr.msg); res.json(jr); return; }

              message.last_attempt = new Date();
              message.attempts++;
              message.status = sendMessageResult.statusCode;

              res.json({"status": "ok", "data": message});

            }, function (err) {
              jr.msg += err ; console.log(jr.msg); res.json(jr);
            });

          });
        } else {
          jr.msg += utils.mongooseErrorToMessages(error); console.log(jr.msg); res.json(jr);
        }
      });
    });
  }

  function execute(req, res) {
    var jr = {"status": "bad", "msg": "fail executing message. "};
    var body = req.body;

    var destId = req.params.id;
    if (!utils.objectIdIsValid(destId)) { jr.msg += "destination id is invalid" ; console.log(jr.msg); res.json(jr); return; }
    var mesId = req.params.messageId;
    if (!utils.objectIdIsValid(mesId)) { jr.msg += "message id is invalid" ; console.log(jr.msg); res.json(jr); return; }

    console.log("message body",  body);
    models.Destination.findOne({_id:destId, userId: req.currentUser._id},function (err, destination) {
      if (err) { throw err; }
      console.log('Destination found : ', destination);
      if (!destination) { jr.msg += "destination not found" ; console.log(jr.msg); res.json(jr); return; }

      var message = _.find(destination.messages, function (m) {
        return m._id == mesId;
      });
      if (!message) { jr.msg += "message id does not exist" ; console.log(jr.msg); res.json(jr); return; }

      sendMessage(destination, message, req.mongoDbConn).then(function (sendMessageResult) {
        if (err) { jr.msg += err ; console.log(jr.msg); res.json(jr); return; }
        res.json({"status": "ok", "data": sendMessageResult});
      }, function (err) {
        jr.msg += err ; console.log(jr.msg); res.json(jr);
      });
    });
  }

  function get(req, res) {
    var jr = {"status": "bad", "msg": "fail getting message. "};
    var destId = req.params.id;
    if (!utils.objectIdIsValid(destId)) { jr.msg += "destination id is invalid" ; console.log(jr.msg); res.json(jr); return; }
    var mesId = req.params.messageId;
    if (!utils.objectIdIsValid(mesId)) { jr.msg += "message id is invalid" ; console.log(jr.msg); res.json(jr); return; }

    models.Destination.findOne({_id:destId, userId: req.currentUser._id},function (err, destination) {
      if (err) { throw err; }
      console.log('Destination found : ', destination);
      if (!destination) { jr.msg += "destination not found" ; console.log(jr.msg); res.json(jr); return; }
      var message = _.find(destination.messages, function (m) {
        return m._id == mesId;
      });
      if (!message) { jr.msg += "message id does not exist" ; console.log(jr.msg); res.json(jr); return; }
      logSvc.get(message, req.mongoDbConn, function (err, logs) {
        if (err) { jr.msg += err ; console.log(jr.msg); res.json(jr); return; }
        message.url = destination.url;
        message.logs = logs;
        res.json({"status": "ok", "data": message});
      });
    });
  }

  return {
    create: create,
    execute: execute,
    get: get,
    sendMessage: sendMessage
  }
};

