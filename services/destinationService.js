var utils = require('../utils/util');

module.exports = function (models) {
  function create(req, res) {
    var jr = {"status": "bad", "msg": "fail creating a new destination. "};
    var body = req.body;
    try {

      if (!body.protocol) { body.protocol = "http:"; }
      if (!body.port) { body.port = 80; }
      var destination = new models.Destination(body);

      destination.validate(function(error) {
        if (!error) {
          destination.userId = req.currentUser._id;
          destination.save(function (err, destination) {
            console.log('Destination stored : ', destination);
            if (err) { jr.msg+= err; console.log(jr.msg); res.json(jr); }
            var data = {
              "id": destination._id
            };
            res.json({"status": "ok", "data": data});
          })
        } else {
          jr.msg += utils.mongooseErrorToMessages(error); console.log(jr.msg); res.json(jr);
        }
      });
    }
    catch (error) {
      jr.msg += error; console.log(jr.msg); res.json(jr);
    }
  }

  function get(req, res) {

    var jr = {"status": "bad", "msg": "fail getting destination. "};
    try {
      var id = req.params.id;

      if (!id) { throw "id is required"; }
      if (!utils.objectIdIsValid(id)) { res.json({"status": "ok", "data": {}}); return; }

      models.Destination.findOne({_id:id, userId: req.currentUser._id}, function (err, destination) {
        if (err) { throw err; }
        console.log('Destination found : ', destination);
        res.json({"status": "ok", "data": destination});
      });
    }
    catch (error) {
      jr.msg += error;
      console.log(jr.msg);
      res.json(jr);
    }
  }

  function getAll(req, res) {

    var jr = {"status": "bad", "msg": "fail getting destinations. "};
    try {
      models.Destination.find({userId: req.currentUser._id}, "-messages", function (err, data) {
        if (err) { throw err; }
        console.log('Destinations found : ', data);
        res.json({"status": "ok", "data": data});
      });
    }
    catch (error) {
      jr.msg += error;
      console.log(jr.msg);
      res.json(jr);
    }
  }

  function remove(req, res) {

    var jr = {"status": "bad", "msg": "fail removing destinations. "};
    try {
      var id = req.params.id;
      if (!id) { throw "id is required"; }
      if (!utils.objectIdIsValid(id)) { throw "id does not exists"; }

      models.Destination.remove({_id:id, userId: req.currentUser._id}, function (err, data) {
        if (err) {
          throw err;
        }
        console.log('destination removed :', data);
        res.json({"status": "ok", "data": data});
      });
    }
    catch (error) {
      jr.msg += error;
      console.log(jr.msg);
      res.json(jr);
    }
  }

  function update(req, res) {

    var jr = {"status": "bad", "msg": "fail updating destination. "};
    var id = req.params.id;
    var body = req.body;

    if (!id) { jr.msg += "id is required" ; console.log(jr.msg); res.json(jr); return; }
    if (!utils.objectIdIsValid(id)) { res.json({"status": "ok", "data": {}}); return; }

    models.Destination.findOne({_id:id, userId: req.currentUser._id}, function (err, destination) {
      if (err) { throw err; }
      console.log('Destination found : ', destination);
      if (!destination) { jr.msg += "destination not found" ; console.log(jr.msg); res.json(jr); return; }

      destination.update(body, function (err, data) {
        if (err) { jr.msg += err ; console.log(jr.msg); res.json(jr); return; }
        res.json({"status": "ok", "data": data});
      });
    });
  }

  return {
    get: get,
    getAll: getAll,
    remove: remove,
    create: create,
    update: update
  }
};