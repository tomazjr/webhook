var constants = require('../utils/constants');

module.exports = function (io) {
  var module = {};

  this.io = io;

  var connections = {};
  var possibleRooms = [];

  var nsp = io.of('/'); // use default namespace, but be explicit for future added namespaces

  module.emit = function (room, message, data) {
    nsp.emit(message, data);
  };

  nsp.on('connection', function (socket) { // client:'connect', server: 'connection'
    console.log('new client connected');

    // socket.on(constants.NOTIFICATION_EVENTS.GATHERING_UPDATE, function (gathering) {
    //     console.log(constants.NOTIFICATION_EVENTS.GATHERING_UPDATE + ' event called');
    //     socket.broadcast.emit(constants.NOTIFICATION_EVENTS.GATHERING_UPDATE, gathering);
    // });

  });

  return module;
};
