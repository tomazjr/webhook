module.exports = function(models) {

  function _getLogTableName(message) {
    return "log_" + message._id;
  }

  function _getLogTable(mongoDbConn, message) {
    var logCollectionName = _getLogTableName(message);
    return mongoDbConn.model(logCollectionName, models.LogSchema, logCollectionName);
  }

  function create(message, data, mongoDbConn, callback) {
    var logTable = _getLogTable(mongoDbConn, message);
    var log = new logTable(data);
    log.save(callback);
  }

  function get(message, mongoDbConn, callback) {
    var logTable = _getLogTable(mongoDbConn, message);
    logTable.find(callback);
  }

  return {
    create : create,
    get: get
  }
};