module.exports = function(cfg) {

  function ping(req, res) {
    res.send(cfg.app.name + ' is at ' + cfg.app.url + '/api' +
      '<p>To see documentation access: ' + cfg.app.url + '/help</p>');
  }

  function help(req, res) {
    res.send('TODO: show a md file content');
  }

  return {
    ping : ping,
    help : help
  }
};