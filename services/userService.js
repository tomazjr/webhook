var jwt    = require('jsonwebtoken');
var utils = require('../utils/util');

module.exports = function(models, cfg) {

  function create(req, res) {

    var jr = {"status": "bad", "msg": "fail creating user. "};

    var userBody = req.body;
    if (!userBody.email || !userBody.password) { jr.msg += "email is required" ; console.log(jr.msg); res.json(jr); return; }

    models.User.findOne({email:userBody.email}, function (err, userDB) {
      if (err) { jr.msg+= err; console.log(jr.msg); res.json(jr); }
      if (userDB) { jr.msg+= "user already exist."; console.log(jr.msg); res.json(jr); }

      var user = new models.User(userBody);
      user.validate(function(error) {
        if (!error) {
          user.password = user.generateHash(user.password);
          user.save(function (err, user) {
            console.log('User stored : ', user);
            if (err) { jr.msg+= err; console.log(jr.msg); res.json(jr); }

            res.json({"status": "ok", "data": null});
          })
        } else {
          jr.msg += utils.mongooseErrorToMessages(error); console.log(jr.msg); res.json(jr);
        }
      });
    });
  }

  function getAll(req, res) {

    var jr = {"status": "bad", "msg": "fail getting users. "};
    models.User.find({}, "-password", function (err, users) {
      if (err) { jr.msg += err ; console.log(jr.msg); res.json(jr); return; }
      res.json({"status": "ok", "data": users});
    });
  }

  function authenticate(req, res) {

    var jr = {"status": "bad", "msg": "Authentication failed. "};

    var user = req.body;
    if (!user.email || !user.password) { jr.msg += "email is required" ; console.log(jr.msg); res.json(jr); return; }

    // find the user
    models.User.findOne({email: user.email}, function(err, user) {
      if (err) { jr.msg += err ; console.log(jr.msg); res.json(jr); return; }
      if (!user) {jr.msg += "user not found." ; console.log(jr.msg); res.json(jr); return;}
      // check if password matches
      if (!user.validPassword(req.body.password)) {jr.msg += "wrong password." ; console.log(jr.msg); res.json(jr); return;}

      // if user is found and password is right
      // create a token
      var token = jwt.sign(user, cfg.secret, {
        expiresIn: 60*60*24 // expires in 24 hours
      });

      res.json({"status": "ok", "data": token});
    });
  }

  return {
    getAll: getAll,
    authenticate: authenticate,
    create: create
  }
};