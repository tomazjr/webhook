var supertest = require('supertest');
var app = require('../../../server');
var user = {}

describe('Login Integration Test', function () {

  it('should be created', function(done){
    supertest(app)
      .post('/user')
      .send({
        "email": "tomaz@gmail.com",
        "password": "123"
      })
      .set('Content-Type', 'application/json')
      .expect(function (res) {
        res.body.should.have.property('status', 'ok');
        res.body.should.have.property('data');
        user.token = res.body.data 
      })
      .end(done);
  })

  it('should be Autenticate', function(done){
    supertest(app)
      .post('/user/authenticate')
      .send({
        "email": "tomaz@gmail.com",
        "password": "123"
      })
      .set('Content-Type', 'application/json')
      .expect(function (res) {
        res.body.should.have.property('status', 'ok');
        res.body.should.have.property('data');
        user.token = res.body.data 
      })
      .end(done);
  })

}) 

module.exports.user = user