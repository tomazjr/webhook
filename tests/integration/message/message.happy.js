var supertest = require('supertest');
var should = require('chai').should();
var app = require('../../../server');
var user = require('../authenticate/authenticate').user;
var destination = require('../destination/destination.happy').destination;

describe('Messages Test Happy Way', function () {

  it('should be created', function(done){
    supertest(app)
      .post('/api/destination/'+destination.id+'/message')
      .set('x-access-token', user.token)
      .send({ path: "www.google.com" })
      .expect(200)
      .expect(function(res) {
        res.body.should.have.property('status', 'ok');
        res.body.should.have.property('data');
      })
    .end(done);
  });

  it('should be listed', function(done){
    supertest(app)
      .get('/api/destination/'+destination.id+'')
      .set('x-access-token', user.token)
      .expect(200)
      .expect(function(res) {
        res.body.should.have.property('status', 'ok');
        res.body.should.have.property('data');
      })
    .end(done);
  });
});