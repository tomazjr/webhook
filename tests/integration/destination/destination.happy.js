var supertest = require('supertest');
var should = require('chai').should();
var app = require('../../../server');
var user = require('../authenticate/authenticate').user;
var destination = {}

describe('Destinations Test Happy Way', function () {


  it('should include', function(done){
    supertest(app)
      .post('/api/destination')
      .send({ url: "www.google.com" })
      .set('x-access-token', user.token)
      .expect(200)
      .expect(function(res) {
        res.body.should.have.property('status', 'ok');
        res.body.should.have.property('data');
        var destination = res.body.data.id
      })
    .end(done);
  });  

  it('should list', function(done){
    supertest(app)
      .get('/api/destinations')
      .set('x-access-token', user.token)
      .expect(function(res) {
        res.body.should.have.property('status', 'ok');
        res.body.should.have.property('data');
      })
    .end(done);
  });
});

module.exports.destination = destination;