
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function objectIdIsValid(id) {
  return id.match(/^[0-9a-fA-F]{24}$/);
}

function mongooseErrorToMessages(error) {
  return Object.keys(error.errors).map(function (err) {
    return error.errors[err].message;
  });
}

module.exports = {
  getRandomInt : getRandomInt,
  objectIdIsValid: objectIdIsValid,
  mongooseErrorToMessages : mongooseErrorToMessages
};