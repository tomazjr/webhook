global.String.prototype.toBoolean = function () {
  return (this == !1 || this == "true");
};

global.String.prototype.startsWith = function (str) {
  return this.indexOf(str) === 0;
};
