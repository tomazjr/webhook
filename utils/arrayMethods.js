global.Array.prototype.firstOrDefault = function () {
  if (this.length)
    return this[0];
  else
    return {};
};

global.Array.prototype.removeById = function (id) {
  var newArray = this.filter(function (obj) {
    return obj.id !== id;
  });

  return newArray;
};
