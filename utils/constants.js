var constants = function () {

  return {
    dateTimeFullFmt: 'YYYY-MM-DD hh:mm:ss'
  };
};

module.exports = constants();